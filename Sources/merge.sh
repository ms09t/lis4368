#!/bin/bash

cd /home/app-ci/.jenkins/workspace/BackendDBTest/Sources

FILE="../Release/release_merge.sql"
if [ -f $FILE ] ; then rm $FILE
	echo "Removing Existing Release File"
fi
	echo "No Release File Pre-exists"
FILES="0.Predeployment/* 1.Table/* 2.Triggers/* 3.Functions/* 4.Views/* 5.StoredProcedure/* 6.Others/* 7.Permissions/*"

OUTPUT="../Release/release_merge.sql"

for f in $FILES
do
    (cat "${f}"; echo) >> $OUTPUT
done
echo "Release File Has Been Created!"